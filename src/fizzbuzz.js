const fizzbuzz = (num) => {
    if (num % 7 == 0 && num % 5 == 0) {
        return "fizzbuzz"
    }

    if (num % 5 == 0) {
        return "buzz"
    }

    if (num % 7 == 0) {
        return "fizz"
    }

    if (num && (num % 7 !== 0 || !num % 5 !== 0)) {
        return ""
    }
    return 'Error!'
}



exports.fizzbuzz = fizzbuzz;