const expect = require('chai').expect;
const { calc } = require('../src/calc');


describe("Calc", () => {
    it("Should return additional result", () => {
        expect(calc(3, 4)).to.equal(7);
    })
})